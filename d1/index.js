console.log("Hello World")

// JSON Objects
/*
	- JSON = JavaScript Object Notation 
	- is also used in other  programming languages
	- JavaScript objects are not to be confused with JSON 
	- serialization is the process of converting data into a series of bytes for easier transmission/transfer of information 
	- uses double quotes for property names
	SYNTAX
		{
			"propertyA": "valueA";
			"propertyB": "valueB";
		}
*/

// JSON Objects
/*
{
	"city": "Quezon City";
	"province": "Metro Manila";
	"country": "Philippines;"
}
*/

// JSON Arrays
/*
	"cities": [
	{ "city": "Quzon City", "province": " Metro Manila", "country"; "Philippines"}
	{ "city": "Manila", "province": " Metro Manila", "country"; "Philippines"}
	{ "city": "Makati", "province": " Metro Manila", "country"; "Philippines"}
	]
*/

// JSON Methods
/*
	- contains methods for parsing and converting data into stringified JSON
*/

// Converting data into stringified JSON
/*
	- stringified JSON is a JavaScript Object converted into a string used in other functions of JavaScript application 
*/

let batchesArr = [{batchname: 'BatchX'}, {batchName: 'BatchY'}]

// The "stringify" method is used to convert JavaScript object into a string

console.log("Result from stringify method:" );
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
	name: 'John',
	age: 31,
	address: {
		city: 'Manila',
		country: 'Philippines'
	}
})
console.log(data);

// Using stringify method with variables
/*
	- when information is stored in a variable and i not hard coded into an object that is being stringified, we can supply the value with a variable
	- this is commonly used when the information to be stired and sent to a backend application will come from a frontend application
	SYNTAX
		JSON.stringify({
	propertyA: variableA,
	propertyB: variableB
		});
*/

//  User details

let firstName = prompt('What is your first name?')
let lastName = prompt('What is your last name?')
let age = prompt('What is your age?')
let address = { 
	city: prompt('Which city do you live in?'),
	country: prompt('What country does your city address belong to?'),
};
let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
})
console.log(otherData);

// Converting stringify JSON into JavaScript objects
/*
	- objects are common data types used in applications because of the complex data structures that can be created out of them
	- information is commonly sent to applications in stringiy JSON and then converted back into objects
	this happens both for sending informtion to a back end application and vice versa
*/

let batchesJSON = `[{"batchName": "BatchX"}, {"batchName": "BatchY"}]`;
console.log("Result from parse method");
	console.log(JSON.parse(batchesJSON));

let stringifiedObject = `{"name": "John", "age": "31", "address": {"city": "Manila", "country": "Philippines"}}`
console.log(JSON.parse(stringifiedObject));